<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use app\models\Entradas;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Entradas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entradas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Nueva entrada', ['create'], ['class' => 'btn btn-dark']) ?>
    </p>


    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'], //Numero de serie
            //Campos de la tabla.
            'id',
            'titulo',
            'texto',
            'fecha',
            //Fin de los campos de la tabla.
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Entradas $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                },
                'buttonOptions' => [//Agrega opciones html a los botones.
                    'class' => 'boton',
                ],
            ],
        ],
    ]);
    ?>


</div>
