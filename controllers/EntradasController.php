<?php

namespace app\controllers;

use app\models\Entradas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EntradasController implements the CRUD actions for Entradas model.
 */
class EntradasController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Entradas models.
     *
     * @return string
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Entradas::find(), // select * from entradas
                /*
                  'pagination' => [
                  'pageSize' => 50
                  ],
                  'sort' => [
                  'defaultOrder' => [
                  'id' => SORT_DESC,
                  ]
                  ],
                 */
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Entradas model.
     * @param int $id Codigo de Entrada
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id), //select * from entradas where id=$id
                        /* 'model' => Entradas::findOne([
                          'id' => $id
                          ]), */
        ]);
    }

    /**
     * Creates a new Entradas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        //Creo un objeto de tipo Entradas vacio.
        $model = new Entradas();

        //Si he apretado el boton de enviar.
        if ($this->request->isPost) {
            if ($model->load($this->request->post())/* Asignacion masiva al modelo de los escritos en el formulario */ && $model->save()/* Intenta almacenar el registro en la base de datos */) {
                //Aqui solo llega si el registro ya esta en la tabla de la base de datos.
                //Redirecciono la aplicacion a la accion view y le paso el id de la entrada generada.
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else { //Si no he apretado el boton d enviar.
            $model->loadDefaultValues();
        }
        //Cargando el formulario con el modelo de entradas generado.
        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Entradas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id Codigo de Entrada
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        //Creo un modelo con los datos de la entrada que tiene la id pasada como argumento.
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Entradas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id Codigo de Entrada
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Entradas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id Codigo de Entrada
     * @return Entradas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Entradas::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('El registro solicitado no existe.');
    }

    public function actionListar() {
        //Crear un activedaraprovider, un elemento que necesita el wiget gridview.
        $dataProvider = new ActiveDataProvider([
            "query" => Entradas::find()
        ]);

        //Llamo a la vista que tiene el gridview y le paso el dataProvider.
        return $this->render('listar', [
                    'datos' => $dataProvider
        ]);
    }

}
